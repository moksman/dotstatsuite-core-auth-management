# HISTORY

##  v3.0.1 2020-01-29 (Uses CommonDb v2.1)
This release contains breaking changes with changes to the authentication management.

- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66
- /sis-cc/.stat-suite/dotstatsuite-core-auth-management/issues/7