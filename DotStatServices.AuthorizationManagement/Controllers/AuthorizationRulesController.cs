﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Common.Model;
using Microsoft.AspNetCore.Http;

namespace DotStatServices.AuthorizationManagement.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiVersion("1.1")]
    public class AuthorizationRulesController : Controller
    {
        private readonly IAuthorizationManagement _authorizationManagement = null;
        private readonly IAuthConfiguration _authConfiguration;
        private DotStatPrincipal _principal;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contextAccessor"></param>
        /// <param name="authorizationManagement"></param>
        /// <param name="configuration"></param>
        public AuthorizationRulesController(IHttpContextAccessor contextAccessor, IAuthorizationManagement authorizationManagement, IAuthConfiguration authConfiguration)
        {
            _authorizationManagement = authorizationManagement;
            _authConfiguration = authConfiguration;
            _principal = new DotStatPrincipal(contextAccessor.HttpContext.User, authConfiguration.ClaimsMapping);
        }

        /// <summary>
        /// List all authorization rules
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{version:apiVersion}/AuthorizationRules")]
        public ActionResult<OperationResult<IEnumerable<UserAuthorization>>> GetAll()
        {
            var callerRules = _authorizationManagement.UserRules(_principal);

            var allRules = _authConfiguration.Enabled
                ? _authorizationManagement.AllRules().Where(x=>x.InScope(callerRules))
                : callerRules;

            return new OperationResult<IEnumerable<UserAuthorization>>(true, allRules);
        }
        
        /// <summary>
        /// Get an authorization rule by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{version:apiVersion}/AuthorizationRules/{id}")]
        public ActionResult<OperationResult<UserAuthorization>> GetById(int id)
        {
            var callerRules = _authorizationManagement.UserRules(_principal);
            var rule = _authorizationManagement.GetAuthorizationRule(id);

            if (rule != null && !rule.InScope(callerRules))
                rule = null;

            return new OperationResult<UserAuthorization>(rule != null, rule);
        }

        /// <summary>
        /// Check if the current User is authorized for a specific scope.
        /// </summary>
        /// <param name="dataSpace"></param>
        /// <param name="artefactAgencyId"></param>
        /// <param name="artefactId"></param>
        /// <param name="artefactVersion"></param>
        /// <param name="requestedPermission"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{version:apiVersion}/AuthorizationRules/IsAuthorized")]
        public ActionResult<OperationResult> IsAuthorized(
            [Required] string dataSpace,
            [Required] string artefactAgencyId,
            [Required] string artefactId,
            [Required] string artefactVersion,
            [Required] PermissionType requestedPermission
        )
        {
            if (!_authConfiguration.Enabled)
                return new BadRequestObjectResult(new OperationResult(false, "Not applicable"));
            
            var result = _authorizationManagement.IsAuthorized(_principal, dataSpace, artefactAgencyId, artefactId, artefactVersion, requestedPermission);

            return new OperationResult(result);
        }

        /// <summary>
        /// Create/update an authorization rule
        /// </summary>
        /// <param name="rule"></param>
        [HttpPost]
        [Route("{version:apiVersion}/AuthorizationRules")]
        public ActionResult<OperationResult> Post([FromBody] UserAuthorization rule)
        {
            if (!ModelState.IsValid)
                return new BadRequestObjectResult(OperationResult.Error(ModelState.GetErrors()));

            if(rule.Id > 0 && _authorizationManagement.GetAuthorizationRule(rule.Id) == null)
                return new BadRequestObjectResult(OperationResult.Error($"There is no rule with ID: {rule.Id}"));

            // Admin cannot save rule with higher scope then he has now
            var canSave = _authorizationManagement.IsAuthorized(
                _principal,
                rule.DataSpace,
                rule.ArtefactAgencyId,
                rule.ArtefactId,
                rule.ArtefactVersion,
                PermissionType.AdminRole
            );

            if (!canSave)
                return StatusCode(403);

            rule.EditedBy = _principal.UserId ?? "Unknown";
            rule.EditDate = DateTime.Now;

            var result =_authorizationManagement.SaveAuthorizationRule(rule);

            return new OperationResult(result > 0);
        }

        /// <summary>
        /// Delete an authorization rule
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        [Route("{version:apiVersion}/AuthorizationRules/{id}")]
        public ActionResult<OperationResult> Delete(int id)
        {
            var rule = _authorizationManagement.GetAuthorizationRule(id);

            if (rule == null)
                return new BadRequestObjectResult(OperationResult.Error($"There is no rule with ID: {id}"));

            // Admin cannot delete rule with higher scope then he has now
            var canDelete = _authorizationManagement.IsAuthorized(
                _principal,
                rule.DataSpace,
                rule.ArtefactAgencyId,
                rule.ArtefactId,
                rule.ArtefactVersion,
                PermissionType.AdminRole
            );

            if (!canDelete)
                return StatusCode(403);

            var result = _authorizationManagement.DeleteAuthorizationRule(id);

            return new OperationResult(result);
        }

        /// <summary>
        /// Get the list of SDMX artefact types
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{version:apiVersion}/GetSDMXArtefactTypes")]
        public ActionResult<OperationResult> GetArtefactTypes()
        {
            var allValues = Enum.GetValues(typeof(SDMXArtefactType))
                .Cast<SDMXArtefactType>()
                .Select(value => new
                {
                    id = value,
                    name = value.ToString()
                });

            return new OperationResult(true, allValues);
        }

        /// <summary>
        /// Get the list of available permissions
        /// </summary>
        [HttpGet]
        [Route("{version:apiVersion}/GetPermissionTypes")]
        public ActionResult<OperationResult> GetPermissionTypes()
        {
            var allValues = Enum.GetValues(typeof(PermissionType))
                .Cast<PermissionType>()
                .Select(value => new
                {
                    id = value,
                    name = value.ToString(),
                    underlying = GetFlags(value).Select(p => p.ToString())
                });

            return new OperationResult(true, allValues);
        }

        /// <summary>
        /// List underlying permissions of a permission: eliminating the given one, None and the redundancies of permissions alrealy included in intermediate ones
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static IEnumerable<Enum> GetFlags(Enum input)
        {
            List<Enum> flags = new List<Enum>();
             
            foreach (Enum value in Enum.GetValues(input.GetType()))
                if (input.HasFlag(value) && value.CompareTo(input) != 0 && value.CompareTo(PermissionType.None) != 0)
                {
                    foreach(Enum val in flags.ToList())
                    {
                        //Remove redundant permision that is included in the current value
                        if (value.HasFlag(val))
                            flags.Remove(val);
                    }

                    flags.Add(value);
                }

            return flags;
        }        
    }
}