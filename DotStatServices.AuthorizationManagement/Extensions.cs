﻿using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Model;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DotStatServices.AuthorizationManagement
{
    public static class Extensions
    {
        public static IEnumerable<string> GetErrors(this ModelStateDictionary modelState)
        {
            return modelState.Values.SelectMany(x => x.Errors.Select(e => e.ErrorMessage));
        }

        public static bool InScope(this UserAuthorization rule, IEnumerable<UserAuthorization> scopeRules)
        {
            if (rule == null || scopeRules == null)
                return false;

            return rule.DataSpace == "*" || scopeRules.Any(x=>x.DataSpace == "*" || x.DataSpace.Equals(rule.DataSpace));
        }
    }
}
