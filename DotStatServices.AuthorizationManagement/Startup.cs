﻿using DryIoc;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStatServices.AuthorizationManagement.HealthCheck;
using DryIoc.Microsoft.DependencyInjection;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotStatServices.AuthorizationManagement
{
    public class Startup
    {
        private readonly AuthConfiguration _auth;

        public Startup(IConfiguration configuration)
        {
            _auth = configuration.GetSection("auth").Get<AuthConfiguration>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;

            // By default, Microsoft has some legacy claim mapping that converts standard JWT claims into proprietary ones.
            // This removes those mappings.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services
                .AddHttpContextAccessor()
                .AddMvc(options =>
                {
                    if (_auth?.Enabled == true)
                    {
                        options.Filters.Add(new AuthorizeFilter());
                    }

                    options.Filters.Add(new ProducesAttribute("application/json"));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult), (int) HttpStatusCode.OK));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult), (int) HttpStatusCode.BadRequest));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int) HttpStatusCode.Unauthorized));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int) HttpStatusCode.Forbidden));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly()));


            services.AddApiVersioning();

            // -----------------------------------------------------------------------

            if (_auth?.Enabled == true)
            {
              services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                }).AddJwtBearer(options =>
                {
                    options.Authority = _auth.Authority;
                    options.Audience = _auth.ClientId;

                    options.RequireHttpsMetadata = _auth.RequireHttps;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = _auth.ValidateIssuer
                    };
                });
            }

            // -----------------------------------------------------------------------

            //This line adds Swagger generation services to our container.
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.1", new Info { Title = "DotStat User Authorizations Management", Version = "1.1" });
                //c.DescribeAllEnumsAsStrings();

                if (_auth?.Enabled == true)
                {
                    c.AddSecurityDefinition("oauth2", new TransferOAuth2Scheme
                    {
                        XTokenName = "id_token",
                        Flow = "implicit",
                        AuthorizationUrl = _auth.AuthorizationUrl,
                        Scopes = _auth.Scopes.ToDictionary(x => x, x => x)
                    });
                    c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>()
                    {
                        { "oauth2", _auth.Scopes}
                    });
                }

                c.DocInclusionPredicate((docName, apiDesc) =>
                {
                    if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo))
                        return false;

                    var versions = methodInfo.DeclaringType
                        .GetCustomAttributes(true)
                        .OfType<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == docName);
                });

                //Locate the XML file being generated by ASP.NET...
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                //... and tell Swagger to use those XML comments.
                c.IncludeXmlComments(xmlPath);
            });

            //https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-2.2
            services.AddHealthChecks()
                .AddCheck<ServiceHealthCheck>("service", tags: new[] { "live" })
                .AddCheck<DbHealthCheck>("database")
                .AddCheck<MemoryHealthCheck>("memory");

            // Dry IOC setup
            return new Container(rules => rules.WithoutThrowIfDependencyHasShorterReuseLifespan())
                .WithDependencyInjectionAdapter(services)
                .ConfigureServiceProvider<ApiIoc>();
        }

        public void Configure(IApplicationBuilder app)
        {
            // Global exception handler
            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = JsonExceptionMiddleware.Invoke
            });

            if (_auth?.Enabled == true)
            {
                app.UseAuthentication();
            }

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.DocumentTitle = "DotStat User Authorizations Management Web API";
                c.SwaggerEndpoint("./v1.1/swagger.json", "DotStat User Authorizations Management Web API");

                if (_auth?.Enabled == true)
                {
                    c.OAuthClientId(_auth.ClientId);

                    // This is hack to request "id_token" instead of hardcoded in Swagger UI "token"
                    c.HeadContent = @"<script>if (!window.isOpenReplaced) {
                        window.open = function(open) {
                            return function(url) {
                                url = url.replace('response_type=token', 'response_type=id_token');
                                url += '&nonce=' + new Date().getTime();
                                return open.call(window, url);
                            };
                        } (window.open);
                        window.isOpenReplaced = true;
                    }
                    </script>";
                }
            });

            app.UseHealthChecks("/health", new DotStatHealthCheckOptions());
            app.UseHealthChecks("/live", new DotStatHealthCheckOptions()
            {
                Predicate = (x) => x.Tags.Contains("live")
            });
        }

        // This is hack class to instruct Swagger sending "id_token" as Bearer instead of default "access_token"
        [ExcludeFromCodeCoverage]
        public class TransferOAuth2Scheme : OAuth2Scheme
        {
            [JsonProperty("x-tokenName")]
            public string XTokenName { get; set; }
        }
    }
}