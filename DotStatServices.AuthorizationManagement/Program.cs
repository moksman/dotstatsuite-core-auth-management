﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace DotStatServices.AuthorizationManagement
{
    [ExcludeFromCodeCoverage]
    public class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));

        public static void Main(string[] args)
        {
            ConfigureLogging();
            CreateWebHostBuilder(args).Build().Run();
        }

        private static void ConfigureLogging()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("config/log4net.config"));
            _log.Info("Application started");
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var configDir = new DirectoryInfo("config");

                    if (!configDir.Exists)
                        throw new InvalidOperationException("Config directory not found");

                    Console.WriteLine($"Config directory name {configDir.FullName}");

                    foreach (var json in configDir.GetFiles("*.json"))
                    {
                        Console.WriteLine($"Configuring from `{json.FullName}` file");
                        config.AddJsonFile(json.FullName);
                    }

                    config.AddEnvironmentVariables();
                })
                .UseStartup<Startup>();
    }
}