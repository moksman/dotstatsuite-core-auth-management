﻿using System.Collections.Generic;
using System.Security.Claims;
using DotStat.Common.Configuration.Interfaces;
using DotStatServices.AuthorizationManagement;
using DotStatServices.AuthorizationManagement.Controllers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace Auth.Test.Unit
{
    [TestFixture]
    public class BootstrapTests
    {
        private IHttpContextAccessor _contextAccessor;

        public BootstrapTests()
        {
            var _email = "mail@host.com";
            var _groups = new[] {"GROUP1", "GROUP2", "GROUP3"};

            var claims = new List<Claim>()
            {
                new Claim("claim/email", _email),
            };

            foreach (var g in _groups)
                claims.Add(new Claim("claim/groups", g));

            var contextMock = new Mock<IHttpContextAccessor>();

            contextMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity(claims)));

            _contextAccessor = contextMock.Object;
        }

        private Startup GetStartup(ServiceCollection services, bool authEnabled = true)
        {
            var builder = new ConfigurationBuilder()
                .AddInMemoryCollection(new[]
                {
                    new KeyValuePair<string, string>("auth:enabled", authEnabled.ToString()),
                    new KeyValuePair<string, string>("auth:claimsMapping:email", "claim/email"),
                    new KeyValuePair<string, string>("auth:claimsMapping:groups", "claim/groups"),
                });

            var configuration = builder.Build();


            services.AddSingleton(_contextAccessor);
            services.AddSingleton<IConfiguration>(configuration);
            services.AddTransient<AuthorizationRulesController>();

            return new Startup(configuration);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ConfigureServices(bool authEnabled)
        {
            var services = new ServiceCollection();
            var startup = GetStartup(services, authEnabled);

            //  Act
            var serviceProvider = startup.ConfigureServices(services);

            //  Assert
            var controller = serviceProvider.GetService<AuthorizationRulesController>();

            Assert.IsNotNull(controller);

            var authConfig = serviceProvider.GetService<IAuthConfiguration>();

            Assert.IsNotNull(authConfig);
            Assert.AreEqual(authEnabled, authConfig.Enabled);

            var jwtOptions = serviceProvider.GetService<IPostConfigureOptions<JwtBearerOptions>>();

            Assert.IsTrue( !authEnabled ^ jwtOptions!=null);
        }

        //[TestCase(true)]
        //[TestCase(false)]
        //public void Configure(bool authEnabled)
        //{
        //    var services = new ServiceCollection();
        //    var startup = GetStartup(services, authEnabled);

        //    var provider = startup.ConfigureServices(services);

        //    var appMock = new Mock<IApplicationBuilder>();

        //    appMock
        //        .SetupGet(a => a.ApplicationServices)
        //        .Returns(provider);

        //    startup.Configure(appMock.Object);
        //}
    }
}
