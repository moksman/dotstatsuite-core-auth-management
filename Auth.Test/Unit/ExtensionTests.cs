﻿using System.Linq;
using DotStatServices.AuthorizationManagement;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NUnit.Framework;

namespace Auth.Test.Unit
{
    [TestFixture]
    public class ExtensionTests
    {
        [Test]
        public void Get_Model_Errors()
        {
            var modelState = new ModelStateDictionary();

            modelState.AddModelError("field-1", "Error message 1-1");
            modelState.AddModelError("field-1", "Error message 1-1");
            modelState.AddModelError("field-2", "Error message 2-1");
            modelState.AddModelError("field-2", "Error message 2-2");

            var errors = modelState.GetErrors();

            Assert.IsNotNull(errors);
            Assert.AreEqual(4, errors.Count());
        }
    }
}
