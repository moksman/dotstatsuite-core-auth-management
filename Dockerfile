FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app

ARG NUGET_FEED=https://api.nuget.org/v3/index.json

# copy only needed files to restore nuget packages
COPY DotStatServices.AuthorizationManagement/*.csproj ./DotStatServices.AuthorizationManagement/
COPY version.json Directory.Build.props ./
WORKDIR /app/DotStatServices.AuthorizationManagement

# restore nuget packages
RUN dotnet restore --source $NUGET_FEED -r linux-x64

# copy everything else
COPY . /app

# publish
RUN dotnet publish -c Release --no-restore -o /out -r linux-x64

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS runtime
WORKDIR /app
COPY --from=build /out .

ENTRYPOINT ["dotnet", "DotStatServices.AuthorizationManagement.dll"]